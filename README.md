# How to start:
1. Use VS Code and clone the repo in the terminal.
2. Enable live server on VS Code

## License information
This application is under the MIT License. This license is short and to the point. It lets people do almost anything they want with your project, like making and distributing closed source versions. 